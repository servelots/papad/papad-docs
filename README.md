# Papad Documentation

## How to write docs

1. To add a new option to the menu, please edit mkdocs.yml and refer to "how to use" section and add files accordingly.
2. To add images (screenshots) store all media in docs/static/section_name/image.png and refer the using markdown.
3. Everyone should write in the draft branch only, changes to prod branch will be rejected.

## Please know

There is currently no writing style followed. Please verify the lanaguge and dont write unnecessary things.

## How to check live :

Open a terminal, and type ```docker-compose up``` . The docker-compose features live reload  so all your changes will reflect live.
## License : CC-By-SA
