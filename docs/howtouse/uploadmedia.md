# Uploading to PAPAD
Upload an audio or video to Papad by using Upload Form with Name, Description,Tags and Media. Where Name and Image are mandatory.

![UploadForm](/static/groups/group_settings.png)

## Uploading a Video/Audio
While uploading a Video we fill the fields and submit.

**Mandatory Fields**:

The Name, Image are mandatory fields are when miss out filling you get a pop up alert to remind.

**Buttons**:

`Upload` and `Cancel`
![UploadForm](/static/uploadmedia/onSubmit.png)

**View Uploaded media**:

`Successfully submitted`
![UploadForm](/static/uploadmedia/postUpload.png)
