# Annotation
Annotation feature allows to talk over a running audio or video, by adding start and end time of the audio, annotating text, with or without annotating image. Would ideally move from image to support multi media - audio, video along with image.

## Annotating a Video
While uploading a Video we fill the fields and submit.

**Annotation Page**:

Start and end time are mandatory, play the video and from the point of time you want to start the annotation click on `Start Time` and similarly when you wanna end annotation click on `End Time` and to next click on the button `annotate` that will open a modal fill the annotation text and tags if required go for upload and you are done !!.
![Annotate](/static/annotate/annotatePage.png)

**Mandatory Fields**:

The Annotation Text is mandatory field, when miss out filling you get a pop up alert to remind. Start and end time are mandatory, whereas MEdia target is not allowed to edit as it comes from start and end time from the annotation page. Tags are optional.
And to annotate submit button `Upload`.
![Annotate](/static/annotate/mandatoryFields.png)

**Buttons**:

`Upload` and `Cancel`, cancel resets the input fields. Upload submits the form.
![Annotate](/static/annotate/onSub.png)

**View Uploaded media**:

`Successfully submitted`
![Annotate](/static/annotate/AnnoSuccess.png)

## Uploading an Audio

Annotating an audio to follows the same procedure just the media format here is Audio.

![Annotate](/static/annotate/AudioAnnoPage.png)

