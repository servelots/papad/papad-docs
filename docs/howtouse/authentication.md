# Authentication

Papad has a elementary authentication system, featuring a username password based login.  

Users can signup to the instance using the following :

1. username
2. password
3. email
4. first name
5. last name

To signup, click on the Login/Signup button in the top of the page, and click on "Create new account" button below and signup

![Singup Button](/static/authentication/user_register.png)

Note: There is currently no feature to authenticate users (like email verification) etc. This is a cautious choice, as papad is usually deployed as a fully offline local network system, so we cant expect users to have access to email. The system's authentication itself is a system of trust and not necessarily a hard way to verify a user.

Once your signup process is complete, you can login.

![Login Button](/static/authentication/user_login.png)


Once you login, your screen will be something like below.

![User Dashboard](/static/authentication/user_dashboard.png)


Please head over to the dashboard page to understand more. 