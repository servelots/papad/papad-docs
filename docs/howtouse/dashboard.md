# Dashboard

Dashboard page gives you an overview of what groups you have access to. to understand groups, open the groups page

Dashboard consists of 2 sections

1. Groups you are a part of
2. Other public groups that are available in the server you are using.

![UploadForm](/static/dashboard/dashboard.png)