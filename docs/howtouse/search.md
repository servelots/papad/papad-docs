# Search
Search is the second best feature we looking at the moment as what you search and get as results fetches the story board inputs so the better the search we provide the better the product will be.

Here we have several options to search,
     
    - Super Search - Directly start searching for 
        - Name
        - Description
        - Location or
        - Author 

    - Search by Name - Click on Name button and start typing the name of the media you looking for and get the result poured down.

    - Search by Description - Click on Description button and start typing the Description of the media you looking for and get the result poured down.

    - Search by Tags - Click on Tags that are on the right side of the screen, on click of the specific tag, renders that tag based media. And to search multi tag just click on the other tags that you looking to search.

![search](/static/search/searchDashboard.png)

## Super Search
Super Search fetches you search from name and description for now. Can search based on name and search based on description. Soon we developing to get you search based on author, location and other fields to give a advance search experience. Search is the second best feature we looking at the moment as what you search and get as results fetches the story board inputs so the better the search we provide the better the product will be.

**Super search based on name**

Search by name
![search](/static/search/superSearchName.png)

**Super search based on name**

Search by description
![search](/static/search/superSearchDesc.png)

## Search by Tags

Search based on tags by on click feature

**Tags search single**

![search](/static/search/searchTags.png)

**Tags search multi**

![search](/static/search/searchTagsMulti.png)

## Search by Name

Search audio by name
![search](/static/search/name.png)
![search](/static/search/nameRes.png)


## Search by Description

Search media by Description
![search](/static/search/desc.png)
![search](/static/search/descRes.png)

## Search by groups
![group search](/static/search/group_search.png)

## Search by Annotations

Search media by Annotations, and allow annotations to be part of story board soon.
![search](/static/search/annotations.png)
![search](/static/search/anno.png)

## Advance Search

It is in processs and yes we are thinking and brain storming and developing one by one on a faster pace to deliver to you folks !!
![search](/static/search/advSearch.png)


