# Papad Documentation

Papad is developed to be a hypermedia annotation tool that can be used across devices and in regions with low internet connectivity and low-literate populations. Tools like papad have significant potential in creating and disseminating knowledge that is audio/video-based, and therefore accessible to populations with lower literacy levels. It can assist anyone in voicing their views on their community related topics. It would be a way to preserve and revive the knowledge for next geneartion. As a tool to share, learn and explore from other communities’ perspectives on art, culture, education, technology, tradition for health and farming. This tool set can be the basis for Audio/Video inclusive Archive and Annotation systems.

More information at https://open.janastu.org/projects/papad

